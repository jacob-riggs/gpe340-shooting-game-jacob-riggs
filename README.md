# GPE340-Shooting-Game-Jacob-Riggs

*Gunfire Sound Effect from: https://opengameart.org/content/the-free-firearm-sound-library

*Level Music from: https://opengameart.org/content/orchestral-battle-music

*Victory Music from: https://opengameart.org/content/victory-fanfare-short

*Loss Music from: https://opengameart.org/content/game-over-theme

*Menu Music from: https://opengameart.org/content/tragic-ambient-main-menu
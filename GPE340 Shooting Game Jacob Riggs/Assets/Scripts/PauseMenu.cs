﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

    public static bool gameIsPaused = false;

    public GameObject pauseMenu;

	// Update is called once per frame
	void Update () {
        // Check for if the player presses escape
        if (Input.GetKeyDown(KeyCode.Escape)) {
            // Resumes the game if the game is paused
            if (gameIsPaused) {
                Resume();
            }
            // Pauses the game if it isn't
            else {
                Pause();
            }
        }
	}

    // Function to resume the game. Turns off the pause menu and sets timescale back to 1. Sets gameIsPaused to false
    public void Resume() {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    // Function to pause the game. Turns on the pause menu and sets the timescale to 0. Sets gameIsPaused to true.
    void Pause() {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

    // Function for the button to go back to the main menu. Also sets the timescale back to 1 so the menu isn't frozen
    public void LoadMenu() {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    // Function to quit the game from the pause menu
    public void QuitGame() {
        Application.Quit();
    }

}

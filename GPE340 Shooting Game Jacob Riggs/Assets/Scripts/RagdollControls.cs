﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RagdollControls : MonoBehaviour {

    public GameObject objectToApplyRagdoll;

    // Things to turn off when we ragdoll, on when normal
    public Collider mainCollider;
    public Animator anim;
    public Rigidbody mainRigBody;
    public NavMeshAgent agent;

    // THings to turn on when ragdoll, off when normal
    public List<Rigidbody> partRigBody;
    public List<Collider> partColliders;

	// Use this for initialization
	void Start () {
        mainCollider = objectToApplyRagdoll.GetComponent<Collider>();
        anim = objectToApplyRagdoll.GetComponent<Animator>();
        mainRigBody = objectToApplyRagdoll.GetComponent<Rigidbody>();
        agent = objectToApplyRagdoll.GetComponent<NavMeshAgent>();

        partRigBody = new List<Rigidbody>(objectToApplyRagdoll.GetComponentsInChildren<Rigidbody>());
        partColliders = new List<Collider>(objectToApplyRagdoll.GetComponentsInChildren<Collider>());
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.P)) {
            ActivateRagdoll();
        }
        if (Input.GetKeyDown(KeyCode.O)) {
            DeactivateRagdoll();
        }
    }

    public void ActivateRagdoll() {
        foreach (Rigidbody rb in partRigBody) {
            rb.isKinematic = false;
        }

        foreach (Collider col in partColliders) {
            col.enabled = true;
        }

        mainCollider.enabled = false;
        mainRigBody.isKinematic = true;
        anim.enabled = false;
        agent.enabled = false;
    }

    public void DeactivateRagdoll() {
        foreach (Collider col in partColliders) {
            col.enabled = false;
        }
        foreach (Rigidbody rb in partRigBody) {
            rb.isKinematic = true;
        }

        mainCollider.enabled = true;
        mainRigBody.isKinematic = false;
        anim.enabled = true;
        agent.enabled = true;

    }

}

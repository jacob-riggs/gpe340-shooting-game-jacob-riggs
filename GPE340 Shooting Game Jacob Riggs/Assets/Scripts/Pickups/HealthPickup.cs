﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class HealthPickup : Pickup {

    public float healAmount;

    protected override void OnPickup(GameObject target)
    {

        Health targetHealth = target.GetComponent<Health>();
        if (targetHealth == null) {
            // Do Nothing
        }
        else {
            targetHealth.Heal(healAmount);
        }

        base.OnPickup(target);
    }

}

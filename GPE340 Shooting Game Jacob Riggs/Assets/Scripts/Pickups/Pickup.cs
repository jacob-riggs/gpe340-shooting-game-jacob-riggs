﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract class Pickup : MonoBehaviour {

    private Transform tf;
    public int rotateSpeed = 360;

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        Spin();
	}

    void Spin() {
        tf.Rotate(0, rotateSpeed * Time.deltaTime, 0);
    }

    protected virtual void OnPickup(GameObject target) {

        // Whatever all pickups do when we call their base.OnPickup
        Debug.Log(target.name + " picked up " + gameObject.name);

        Destroy(gameObject);
    }

    public void OnTriggerEnter(Collider other) {

        OnPickup(other.gameObject);

    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class WeaponPickup : Pickup {

    // Variable to store which weapon a pickup is
    public Weapon weapon;

    protected override void OnPickup(GameObject target) {
        WeaponController targetWeaponController = target.GetComponent<WeaponController>();
        if (targetWeaponController != null) {
            targetWeaponController.Unequip();
            targetWeaponController.EquipWeapon(weapon);
            base.OnPickup(target);
        }
    }

}

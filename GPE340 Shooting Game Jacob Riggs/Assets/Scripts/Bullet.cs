﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    // Get the health script
    private Health health;

    // Bullet information
    public Transform tf;
    public float moveSpeed;
    public float lifespan;
    public float damage;

	// Use this for initialization
	void Start () {
        health = GetComponent<Health>();
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        Move();
        DestroyObjectDelayed();
	}

    // Moves the bullet forward

    public void Move() {
        tf.position += (tf.forward * moveSpeed) * Time.deltaTime;
    }

    // Destroys the object after a set amount of time

    public void DestroyObjectDelayed() {
        Destroy(this.gameObject, lifespan);
    }

    // Damages another object if it hits it. Still working on refining this

    void OnTriggerEnter(Collider other) {
        other.gameObject.GetComponent<Health>().health -= damage;
        Destroy(this.gameObject);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    // Variables to have camera track and follow player
    public Transform targetObjectTransform;
    public Vector3 offset;

    private Transform tf;

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        // Sets the camera a certain offset number from the player
        tf.position = targetObjectTransform.position + offset;
        // Makes sure to look at the player and not in the original set direction
        tf.LookAt(targetObjectTransform.position);
	}
}

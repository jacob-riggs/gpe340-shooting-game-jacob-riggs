﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class test : MonoBehaviour {

    [SerializeField] private int _health;
    public int health {
        get {
            return (int)(((float)_health / (float)maxHealth) * 100);
        }
        set {
            _health = Mathf.Clamp(value, 0, maxHealth);
            healthText.text = _health + "%";
        }
    }

    public int maxHealth;
    public Text healthText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControler : MonoBehaviour {

    // Grabs the pawn and all of its variables and functions
    public Pawn pawn;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        // Will perform the functions every frame, but only if they are performed
        Rotation();
        Movement();
	}

    void Rotation() {
        // Create a Plane object
        Plane thePlane = new Plane(Vector3.up, pawn.tf.position);
        // Racyast out the camera (at mouse position) to the plane -- find the distance to the plane
        float distance;
        Ray theRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        thePlane.Raycast(theRay, out distance);
        // Find a point on the ray that is "distance" down the ray
        Vector3 targetPoint = Camera.main.ScreenPointToRay(Input.mousePosition).GetPoint(distance);
        // Rotate towards that point
        pawn.RotateTowards(targetPoint);
    }

    void Movement ()  {
        // Gets the correct input of where the player is moving so that the function can move them that way
        Vector3 moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
        moveDirection = Vector3.ClampMagnitude(moveDirection, 1.0f);
        moveDirection = pawn.tf.InverseTransformDirection(moveDirection);

        // Moves the pawn in the correct direction
        pawn.Move(moveDirection);
    }
}

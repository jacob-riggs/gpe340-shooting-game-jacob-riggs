﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {


    // Health elements for the player
    [Header("Health UI Elements")]
    public Text livesText;
    public Slider slider;
    public Health health;
    public int lives;

    // UI Elements to display current weapon
    [Header("Weapon UI Elements")]
    public Weapon weaponDisplay;

    // Weapon variables for the player
    [Header("Weapon Controls")]
    public Weapon weapon;
    public WeaponController weaponController;

    // Variable so it knows where to respawn player
    [Header("Respawn Controls")]
    public GameObject startPoint;

    // Use this for initialization
    void Start () {
        // Get needed components
        health = GetComponent<Health>();
        weapon = GetComponent<Weapon>();
        weaponController = GetComponent<WeaponController>();
    }
	
	// Update is called once per frame
	void Update () {
        // Displays the players current health in the corner
        displayHealth();

        // Fires as the player holds space
        if (Input.GetKey(KeyCode.Space) && Time.time > weaponController.equippedWeapon.GetComponent<Weapon>().nextFire) {
            weaponController.equippedWeapon.GetComponent<Weapon>().Shoot();
        }

        // Sets the player back to the starting point if they run out of health
        if (health.health <= 0 && lives > 0) {
            transform.position = startPoint.transform.position;
            lives -=1;
            health.health = health.maxHealth;
        }
        // Takes them to the loss screeen if they run out of lives
        if (health.health <= 0 && lives <= 0) {
            SceneManager.LoadScene(2);
        }
    }

    // Function that constantly changes the livesText and health slider to the current amount of health and lives the player has
    public void displayHealth() {
        livesText.text = "Lives: " + lives;
        slider.value = health.health / 100;
    }

    // Function that checks for the equipped weapon and displays it on the UI

    public void displayWeapon() {
        weaponDisplay = weaponController.equippedWeapon;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class AIController : MonoBehaviour {

    // Get all of the needed variables for the AI to work

    // General variables for the AI to function, move, and die
    public Pawn playerPawn;
    private Pawn pawn;
    private NavMeshAgent agent;
    private Health health;
    public Slider slider;

    // Controls for the AI Weapon

    [Header("Weapon Controls")]
    public Weapon weapon;
    public WeaponController weaponController;
    public GameObject lookPoint;
    public float rayDistance;
    public float lookDistance;

    [Header("Ragdoll Elements")]
    public GameObject objectToApplyRagdoll;

    // Things to turn off when we ragdoll, on when normal
    public Collider mainCollider;
    public Animator anim;
    public Rigidbody mainRigBody;
    public NavMeshAgent agentOff;

    // THings to turn on when ragdoll, off when normal
    public List<Rigidbody> partRigBody;
    public List<Collider> partColliders;

    // Use this for initialization
    void Start () {

        // Get the players Pawn, even if Instaniated to spawn
        playerPawn = GameObject.Find("Player").GetComponent<Pawn>();

        // Get needed components from self
        pawn = GetComponent<Pawn>();
        agent = GetComponent<NavMeshAgent>();
        weaponController = GetComponent<WeaponController>();
        health = GetComponent<Health>();

        // Get components for ragdoll
        mainCollider = objectToApplyRagdoll.GetComponent<Collider>();
        anim = objectToApplyRagdoll.GetComponent<Animator>();
        mainRigBody = objectToApplyRagdoll.GetComponent<Rigidbody>();
        agentOff = objectToApplyRagdoll.GetComponent<NavMeshAgent>();

        partRigBody = new List<Rigidbody>(objectToApplyRagdoll.GetComponentsInChildren<Rigidbody>());
        partColliders = new List<Collider>(objectToApplyRagdoll.GetComponentsInChildren<Collider>());

    }
	
	// Update is called once per frame
	void Update () {
        agent.SetDestination(playerPawn.tf.position);
        agent.destination = playerPawn.tf.position;

        MoveWithRootMotion();

        slider.value = health.health / 100;

        // Get distance from AI to player
        float distance = Vector3.Distance(playerPawn.transform.position, transform.position);

        // Shoot if the player is in the lookdistance
        if(distance <= lookDistance && Time.time > weaponController.equippedWeapon.GetComponent<Weapon>().nextFire) {
            weaponController.equippedWeapon.GetComponent<Weapon>().Shoot();
        }

        // Ragdoll AI when at 0 health, then destroy it so another spawns
        if (health.health <= 0) {
            ActivateRagdoll();
            Destroy(weaponController.equippedWeapon);
            Destroy(this.gameObject, 2f);
        }

    }

    void MoveWithRootMotion() {
        // Get the desired velocity from the NavMesh agent
        Vector3 desiredVelocity = agent.desiredVelocity * pawn.moveSpeed;
        //Make sure our movement is in the correct space
        desiredVelocity = pawn.tf.InverseTransformDirection(desiredVelocity);
        // Set the floats in our animator so root motion uses that velocity
        pawn.anim.SetFloat("Horitzontal", desiredVelocity.x);
        pawn.anim.SetFloat("Vertical", desiredVelocity.z);
    }

    private void OnAnimatorMove() {
        // Make sure we don't double our speed by setting our navMesh agent velocity to root motion determined velocity
        agent.velocity = pawn.anim.velocity;

    }

    // Function to activate the ragdoll effect for the AI.

    public void ActivateRagdoll() {
        foreach (Rigidbody rb in partRigBody) {
            rb.isKinematic = false;
        }

        foreach (Collider col in partColliders) {
            col.enabled = true;
        }

        mainCollider.enabled = false;
        mainRigBody.isKinematic = true;
        anim.enabled = false;
        agentOff.enabled = false;
    }

}

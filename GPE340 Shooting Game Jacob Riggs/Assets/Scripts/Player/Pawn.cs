﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.UI;

public class Pawn : MonoBehaviour {

    // Variables to get the animations and transform for the pawn so it can move and be controlled
    [HideInInspector]public Animator anim;
    [HideInInspector]public Transform tf;

    // Variables to restrict movement
    [Header("Movement")]
    public float turnSpeed;
    public float moveSpeed;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        
        
    }

    // A function to move the pawn
    public void Move(Vector3 direction) {
        // Moves them at a max speed in whatever direction is inputted later
        anim.SetFloat("Vertical", direction.z * moveSpeed);
        anim.SetFloat("Horitzontal", direction.x * moveSpeed);
    }

    // A function that will rotate the pawn towards something, whether it be a mouse or a player
    public void RotateTowards(Vector3 targetPoint) {
        // Finds the targets position and grabs it to be rotated towards
        Vector3 vectorLookDown = targetPoint - tf.position;
        Quaternion lookRotation = Quaternion.LookRotation(vectorLookDown, tf.up);
        // Makes it so the pawn gradually rotates towards the target
        tf.rotation = Quaternion.RotateTowards(tf.rotation, lookRotation, (turnSpeed * Time.deltaTime));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class Health : MonoBehaviour {

    [Header("Health Numbers")]
    /// <summary> A health amount to be kept track of </summary>
    public float health;

    [Tooltip("MaxHealth amount to set")]
    public float maxHealth;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void TakeDamage( float amount ) {
        health -= amount;
    }

    public void Heal( float amount ) {
        health += amount;
    }
}

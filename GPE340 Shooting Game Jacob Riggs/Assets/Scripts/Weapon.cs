﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{


    // Weapon Vairables
    public Transform RHPoint;
    public Transform LHPoint;
    public Transform shotSpawn;

    public List<GameObject> shootPoints;

    // Shooting variables
    public float shootSpeed;
    public GameObject bullet;
    public GameObject particle;
    public AudioClip fire;

    public float nextFire;

    // Use this for initialization
    void Start() {
        
        // A possible start to getting the shotgun to work?

        //shootPoints = new List<GameObject>(GameObject.FindGameObjectsWithTag("ShootPoint"));
    }

    // Update is called once per frame
    void Update() {
        
    }

    // Instantiates a bullet 
    public virtual void Shoot() {
        Instantiate(particle, shotSpawn.position, shotSpawn.rotation);
        Instantiate(bullet, shotSpawn.position, shotSpawn.rotation);
        AudioSource.PlayClipAtPoint(fire, shotSpawn.position);
        nextFire = Time.time + shootSpeed;

    }

}
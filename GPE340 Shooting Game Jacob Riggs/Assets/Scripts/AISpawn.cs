﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISpawn : MonoBehaviour {

    public float respawnTime;
    public GameObject objectToSpawn;
    private GameObject spawnedObject;
    private float timeUntilNextRespawn;
    private Transform tf;
    public GameObject victoryScript;

    private void Awake() {

        tf = GetComponent<Transform>();

    }

    // Use this for initialization
    void Start() {
        //Spawn on start
        Spawn();
    }

    // Update is called once per frame
    void Update() {
        // Check if object exists -- if so, don't countdown
        if (spawnedObject != null) {
            return;
        }

        // If it does not exist
        timeUntilNextRespawn -= Time.deltaTime;

        if (timeUntilNextRespawn <= 0) {
            Spawn();
            victoryScript.GetComponent<AIRemaining>().numOfAI -= 1;
        }


    }

    public void Spawn() {
        // Create my object
        spawnedObject = Instantiate(objectToSpawn, tf.position, tf.rotation);
        // Reset the respawn time
        timeUntilNextRespawn = respawnTime;
    }
}

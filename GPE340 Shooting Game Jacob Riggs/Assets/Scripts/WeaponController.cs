﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]

public class WeaponController : MonoBehaviour {

    // Variables
    public Weapon startWeapon;
    public Transform holdPoint;
    public Weapon equippedWeapon;
    private Animator anim;

	// Use this for initialization
	void Start () {
        anim = gameObject.GetComponent<Animator>();

        EquipWeapon(startWeapon);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Instantiates the weapon on the player at the given positions

    public void EquipWeapon(Weapon weapon) {
        equippedWeapon = Instantiate(weapon) as Weapon;
        equippedWeapon.transform.SetParent(holdPoint);
        equippedWeapon.transform.localPosition = weapon.transform.localPosition;
        equippedWeapon.transform.localRotation = weapon.transform.localRotation;
        // Changes the layer of the weapon if its the player so it can show up on the UI
        if (holdPoint.tag == "Player") {
            equippedWeapon.gameObject.layer = 4;
            foreach (Transform child in equippedWeapon.transform) {
                    child.gameObject.layer = 4;

            }
        }

    }

    // Destroys the old weapon when the new one is equipped

    public void Unequip() {
        if (equippedWeapon) {
            Destroy(equippedWeapon.gameObject);
            equippedWeapon = null;
        }
    }

    // Handles where to put the hands of the player on the weapon given the hold points set beforehand.

    protected void OnAnimatorIK(int layerIndex) {

        if (!equippedWeapon)
            return;
        if (equippedWeapon.RHPoint) {
            anim.SetIKPosition(AvatarIKGoal.RightHand, equippedWeapon.RHPoint.position);
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1f);
            anim.SetIKRotation(AvatarIKGoal.RightHand, equippedWeapon.RHPoint.rotation);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1f);
        }
        else {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0f);
            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 0f);
        }
        if (equippedWeapon.LHPoint) {
            anim.SetIKPosition(AvatarIKGoal.LeftHand, equippedWeapon.LHPoint.position);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1f);
            anim.SetIKRotation(AvatarIKGoal.LeftHand, equippedWeapon.LHPoint.rotation);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1f);
        }
        else {
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0f);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0f);
        }
    }
}
